import pygame
from typing import Dict, Any, List, Optional, Tuple
from pathlib import Path
from dataclasses import dataclass, field
from math import pi, sin, cos, atan2, exp
from random import random

_RAYS = 10
_DELTA_THETA = 2.0*pi/_RAYS
_ANGLES = list([float(i) * _DELTA_THETA for i in range(_RAYS)])
_DELTA_T = 0.001
_MAX_SPEED = 5000.0
_MAX_ANGULAR_SPEED = 2.0*pi * 10000 * _DELTA_T

def sign(x: float):
    if x<0:
        return -1
    return 1

def sigmoid(x: float):
    if x < -1000.0:
        return 1.0
    if x > 1000.0:
        return 0.0
    return 1.0/(1.0 + exp(-x))

class QuitEvent(Exception):
    """ Raised when the program is quit. """

@dataclass
class Neuron:
    bias: float
    weight: float

@dataclass
class Actor:
    sprite: pygame.Surface
    sprite_angle: float
    sprite_rect: pygame.Rect

    linear_velocity: float = 0.0
    angular_velocity: float = 0.0

    neurons: List[Neuron] = field(default_factory=lambda: [Neuron(random(), random()) for _ in range(len(_ANGLES))])
    alive: bool = False
    best_distsqr: float = 100000000000.0

    def __post_init__(self):
        if self.sprite_rect is None:
            self.sprite_rect = self.sprite.get_rect()

screen = None

ball = None
ballrect = None

width = None
height = None

goal: pygame.image = None
goal_rect: pygame.Rect = None
goal_lines: List[Tuple[float, float, float, float]] = None


def get_line_intersection(x1, y1, x2, y2, x3, y3, x4, y4) -> Optional[Tuple[float, float]]:
    denominator = ((x1 - x2) * (y3 - y4)) - ((y1 - y2) * (x3 - x4))
    if denominator < .00001:
        return None

    a1 = (x1*y2)-(y1*x2)
    a2 = (x3*y4)-(y3*x4)
    return (
        (a1*(x3-x4))-((x1-x2)*a2),
        (a1*(y3-y4))-((y1-y2)*a2)
    )

def do_life(actor: Actor):
    angular_velocity = 0
    linear_velocity = 0
    for index, angle in enumerate(_ANGLES):
        x1 = actor.sprite_rect.centerx
        y1 = actor.sprite_rect.centery
        x2 = x1 + cos(angle+actor.sprite_angle)
        y2 = y1 + sin(angle+actor.sprite_angle)
        best = None
        for test_line in goal_lines:
            intersection = get_line_intersection(
                x1, y1, x2, y2,
                test_line[0], test_line[1], test_line[2], test_line[3]
            )
            if intersection:
                dx = intersection[0] - x1
                dy = intersection[1] - y1
                distsqr = 1000000000.0 - (dx * dx + dy * dy)
                if best is None or best < distsqr:
                    best = distsqr

        if best is None:
            best = 0.0

        angular_velocity += actor.neurons[index].bias + best*actor.neurons[index].weight
        linear_velocity += actor.neurons[index].bias + best*actor.neurons[index].weight

    actor.angular_velocity = sigmoid(angular_velocity) * _MAX_ANGULAR_SPEED
    actor.linear_velocity = sigmoid(linear_velocity) * _MAX_SPEED

    if actor.best_distsqr is not None and best > actor.best_distsqr:
        actor.best_distsqr = best

def _loop(actors: List[Actor]) -> bool:
    global black, width, height
    for event in pygame.event.get():
        if event.type == pygame.QUIT: raise QuitEvent

    screen.fill(black)
    for actor in actors:
        do_life(actor)
        actor.sprite_angle += actor.angular_velocity * _DELTA_T
        actor.sprite_rect.move_ip(
            actor.linear_velocity * _DELTA_T * cos(actor.sprite_angle),
            actor.linear_velocity * _DELTA_T * sin(actor.sprite_angle),
        )

        if actor.sprite_rect.left < 0:
            actor.sprite_angle = atan2(sin(actor.sprite_angle), -cos(actor.sprite_angle))
            actor.sprite_rect.left = 0
        if actor.sprite_rect.right > width:
            actor.sprite_angle = atan2(sin(actor.sprite_angle), -cos(actor.sprite_angle))
            actor.sprite_rect.right = width
        if actor.sprite_rect.top < 0:
            actor.sprite_angle = atan2(-sin(actor.sprite_angle), cos(actor.sprite_angle))
            actor.sprite_rect.top = 0
        if actor.sprite_rect.bottom > height:
            actor.sprite_angle = atan2(-sin(actor.sprite_angle), cos(actor.sprite_angle))
            actor.sprite_rect.bottom = height

        screen.blit(actor.sprite, actor.sprite_rect)
    screen.blit(goal, goal_rect)

def randomize_goal():
    global goal_rect, goal, goal_lines
    goal_rect.update(
        random()*(width-goal_rect.width), random()*(height-goal_rect.height),
        goal_rect.width, goal_rect.height
    )

    goal_lines = [
        (goal_rect.left, goal_rect.top, goal_rect.right, goal_rect.top),
        (goal_rect.right, goal_rect.top, goal_rect.right, goal_rect.bottom),
        (goal_rect.right, goal_rect.bottom, goal_rect.left, goal_rect.bottom),
        (goal_rect.left, goal_rect.bottom, goal_rect.left, goal_rect.top),
    ]


def _init() -> Dict[str, Any]:
    global black, screen, width, height, goal_rect, goal, goal_lines
    pygame.init()

    size = width, height = 1920, 1080
    black = (0, 0, 0)

    screen = pygame.display.set_mode(size)

    goal = pygame.image.load(Path(__file__).parent / "intro_ball.gif")
    goal_rect = goal.get_rect()


def add_random_actor(actors: List[Actor]):
    sprite = pygame.image.load(Path(__file__).parent / "intro_ball.gif")
    rect = sprite.get_rect()
    rect2 = rect.move_ip(random()*(width-rect.width), random()*(height-rect.height))
    actors.append(Actor(
        sprite,
        random() * 2.0 * pi,
        rect2
    ))


def reset_actors(actors: List[Actor]):
    for actor in actors:
        actor.sprite_angle = random() * 2.0 * pi
        actor.sprite_rect.update(
            0,0,
            actor.sprite_rect.width, actor.sprite_rect.height
        )

def main():
    _init()
    actors = []
    for _ in range(100):
        add_random_actor(actors)

    try:
        while True:
            reset_actors(actors)
            randomize_goal()
            for _ in range(1000):
                if _loop(actors):
                    break
                pygame.display.flip()

            best_actors = sorted(actors, key=lambda x: x.best_distsqr, reverse=True)
            actors = best_actors[:50]
            for _ in range(50):
                add_random_actor(actors)

    except QuitEvent:
        pass


if __name__=="__main__":
    main()